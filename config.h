/* See LICENSE file for copyright and license details. */

/* appearance */
static const char font[] = FONT;
#define NUMCOLORS 16
static const char colors[NUMCOLORS][ColLast][9] = {
// border     foreground background
{  "#002b36", "#586e75", "#002b36" }, // 01: base01 (inactive)
{  "#586e75", "#93a1a1", "#002b36" }, // 02: base1  (selected)
{  "#002b36", "#002b36", "#002b36" }, // 03: base03
{  "#002b36", "#073642", "#002b36" }, // 04: base02
{  "#002b36", "#657b83", "#002b36" }, // 05: base00
{  "#002b36", "#839496", "#002b36" }, // 06: base0
{  "#002b36", "#eee8d5", "#002b36" }, // 07: base2
{  "#002b36", "#fdf6e3", "#002b36" }, // 08: base3
{  "#002b36", "#b58900", "#002b36" }, // 09: yellow
{  "#002b36", "#c4976c", "#002b36" }, // 0A: orange
{  "#002b36", "#c46c6c", "#002b36" }, // 0B: red
{  "#002b36", "#c46cb9", "#002b36" }, // 0C: magenta
{  "#002b36", "#a19398", "#002b36" }, // 0D: violet
{  "#002b36", "#6c77c4", "#002b36" }, // 0E: blue
{  "#002b36", "#6c9cc4", "#002b36" }, // 0F: cyan
{  "#002b36", "#6cc487", "#002b36" }, // 10: green
};


static const unsigned int borderpx  = BORDERPX; /* border pixel of windows */
static const unsigned int gappx     = GAPPX;    /* gap pixel between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const Bool showbar           = True;     /* False means no bar */
static const Bool topbar            = True;     /* False means bottom bar */
static const Bool showtitle         = False;
static const int defaultlayout      = 0;

/* tagging */
static const char *tags[] =        { "1", "2", "3", "4", "5", "6", "7", "8" };

static const Element elements[] = {
    /* specify elements in status bar in occuring order */
    { "_",                  ClkBarNotif    },
#if COUNTERS > 0
    { "_xxxS",              ClkBarCounter1 },
#if COUNTERS > 1
    { "_xxxS",              ClkBarCounter2 },
#if COUNTERS > 2
    { "_xxxS",              ClkBarCounter3 },
#if COUNTERS > 3
    { "_xxxS",              ClkBarCounter4 },
#if COUNTERS > 4
    { "_xxxS",              ClkBarCounter5 },
#endif
#endif
#endif
#endif
#endif
    { "__xxxx-xx-xx xx:xx", ClkBarDate     },
    { "_x_",                ClkBarMode     },
};

#define SPAWNTYPE     SpawnNewtag
static const Rule rules[] = {
/* class                title               tag       float  nofocus     autosel/spawn    mon         x       y       w       h  */
{  "Alert",             NULL,               0,        1,           1,    NULL,             -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "Wtmux",             NULL,               0,        0,           0,    "console",        -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "Navigator",         "main browser",     0,        0,           0,    "browser",        -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "Navigator",         "email browser",    0,        0,           0,    "email",          -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  NULL,                "terminal",         0,        1,           0,    NULL,             -1,   CENTER, CENTER,    650,    280  },
{  "Wexplorer",         NULL,               0,        0,           0,    "explorer",       -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "Wpassword",         "password",         0,        1,           0,    NULL,             -1,   CENTER, CENTER,    490,    150  },
{  "Wplayer",           NULL,               0,        1,           0,    "player",         -1,   CENTER, CENTER,    802,    600  },
{  NULL,                "viewer",           0,        1,           1,    NULL,             -1,     1200,     50,    640,    480  },
{  "keepassx",          "KeePassX",         0,        1,           0,    NULL,             -1,   CENTER, CENTER,   1100,    600  },
{  "calendarpopup",     "calendar",         0,        1,           0,    NULL,             -1,     1624,     20,    253,    172  },
{  "Wconky",            "conky_diag",       0,        1,           0,    NULL,             -1,   CENTER, CENTER,   AUTO,   AUTO  },
{  "arandr",            NULL,               0,        1,           0,    NULL,             -1,   CENTER, CENTER,   AUTO,   AUTO  },
{  "xv",                NULL,               0,        1,           0,    NULL,             -1,   CENTER, CENTER,   AUTO,   AUTO  },
{  "thunar",            NULL,               0,        0,           0,    "explorer alt",   -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "Wemail",            NULL,               0,        0,           0,    "email alt",      -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "google-chrome",     "Google Chrome",    0,        0,           0,    "browser alt",    -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  "Winv_terminal",     "invtmux",          0,        0,           0,    "console alt",    -1,     AUTO,   AUTO,   AUTO,   AUTO  },
{  NULL,                "Android",          0,        1,           0,    NULL,             -1,   CENTER, CENTER,   AUTO,   AUTO  },
};

#define AUTOWIN_DEFAULT "console"

/* layout(s) */
static const float mfact      = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster      = 1;    /* number of clients in master area */
static const Bool resizehints = False; /* True means respect size hints in tiled resizals */
static const Layout layouts[] = {
    /*        symbol           arrange function  *
     * spawn  spawn   spawn                      *
     * newtag local   floating                   */
    { { " ≡",   " +",   " o"   },  tile    },  /* first entry is default */
    { { " ≡≡",  " ++",  " oo"  },  NULL    },  /* no layout function means floating behavior */
    { { " ≡≡≡", " +++", " ooo" },  monocle },
};
static const char* controlsymbol = " ≡ ";

/* key definitions */
#define MODKEY Mod4Mask
#define AltMask Mod1Mask
#define AltGrMask Mod5Mask
#define ButtonMod  Button9
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tagto,          {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* alternate keys */
#define max_alternates  7
static const KeySym alternates[][max_alternates] = {
 { XK_a, XK_agrave,      XK_acircumflex, XK_adiaeresis,  0,             0,           0 },
 { XK_c, XK_ccedilla,    0,              0,              0,             0,           0 },
 { XK_e, XK_eacute,      XK_egrave,      XK_ecircumflex, XK_ediaeresis, XK_EuroSign, 0 },
 { XK_i, XK_icircumflex, XK_idiaeresis,  0,              0,             0,           0 },
 { XK_o, XK_ocircumflex, XK_odiaeresis,  0,              0,             0,           0 },
 { XK_u, XK_ugrave,      XK_ucircumflex, XK_udiaeresis,  XK_mu,         0,           0 },
};

#define BIN "~/.bin"
#define CLEANUP "~/.bin/dwm_ctrl cleanup"
#define MAX_SPAWNER_LEN 100

static Key keys[] = {
    /* modifier             key                         function        argument */
    { 0,                    XK_Alt_R,                   alternate,      { 0                  }},
    { MODKEY,               XK_d,                       autoselect,     { .v = "console"     }},
    { MODKEY|ShiftMask,     XK_d,                       moveto,         { .v = "console"     }},
    { MODKEY|AltMask,       XK_d,                       autoselect,     { .v = NULL          }},
    { MODKEY|AltMask|ShiftMask, XK_d,                   moveto,         { .v = NULL          }},
    { MODKEY|ControlMask,   XK_d,                       setautoselect,  { .v = "notify info 'auto-window has been redefined'" }},
    { 0,                    XF86XK_Calculator,          autoselect,     { .v = "explorer"    }},
    { MODKEY|ShiftMask,     XF86XK_Calculator,          moveto,         { .v = "explorer"    }},
    { AltMask,              XF86XK_Calculator,          autoselect,     { .v = "explorer alt"}},
    { 0,                    XF86XK_Mail,                autoselect,     { .v = "email"       }},
    { MODKEY|ShiftMask,     XF86XK_Mail,                moveto,         { .v = "email"       }},
    { AltMask,              XF86XK_Mail,                autoselect,     { .v = "email alt"   }},
    { 0,                    XF86XK_HomePage,            autoselect,     { .v = "browser"     }},
    { AltMask,              XF86XK_HomePage,            autoselect,     { .v = "browser alt" }},
    { MODKEY|ShiftMask,     XF86XK_HomePage,            moveto,         { .v = "browser"     }},
    { MODKEY,               XF86XK_AudioPlay,           autoselect,     { .v = "player"      }},
    { ControlMask,          XF86XK_AudioPlay,           spawn,          { .v = "stop"        }},
    { 0,                    XF86XK_AudioPlay,           spawn,          { .v = "play"        }},
    { AltMask,              XF86XK_AudioPlay,           spawn,          { .v = "radio"       }},
    { ShiftMask,            XF86XK_AudioPlay,           spawn,          { .v = "play 1"      }},
    { 0,                    XF86XK_AudioPrev,           spawn,          { .v = "prev"        }},
    { 0,                    XF86XK_AudioNext,           spawn,          { .v = "next"        }},
    { 0,                    XF86XK_Eject,               spawn,          { .v = "osd ~/.bin/rmount" }},
    { MODKEY,               XK_bracketright,            spawn,          { .v = "seek +5"     }},
    { MODKEY,               XK_bracketleft,             spawn,          { .v = "seek -5"     }},
    { 0,                    XF86XK_AudioMute,           spawn,          { .v = "mute"        }},
    { 0,                    XF86XK_AudioLowerVolume,    spawn,          { .v = "volume down" }},
    { 0,                    XF86XK_AudioRaiseVolume,    spawn,          { .v = "volume up"   }},
    { ShiftMask,            XF86XK_AudioLowerVolume,    spawn,          { .v = "brightness down" }},
    { ShiftMask,            XF86XK_AudioRaiseVolume,    spawn,          { .v = "brightness up" }},
    { 0,                    XK_Print,                   spawn,          { .v = "screenshot all" }},
    { ShiftMask,            XK_Print,                   spawn,          { .v = "screenshot win" }},
    { ControlMask,          XK_Print,                   spawn,          { .v = "screenshot manual" }},
    { MODKEY,               XK_Menu,                    spawn,          { .v = "terminal"    }},
    { MODKEY|AltMask,       XK_Menu,                    spawn,          { .v = "terminal alt"}},
    { MODKEY|ControlMask,   XK_Menu,                    autoselect,     { .v = "console alt" }},
    { 0,                    XK_Menu,                    spawn,          { .v = "menu alt"    }},
    { AltMask,              XK_Menu,                    spawn,          { .v = "menu"        }},
    { MODKEY,               XK_z,                       spawn,          { .v = "menu alt"    }},
    { MODKEY|AltMask,       XK_z,                       spawn,          { .v = "menu"        }},
    { MODKEY,               XK_F1,                      autoselect,     { .v = "console"     }},
    { MODKEY,               XK_F2,                      autoselect,     { .v = "browser"     }},
    { MODKEY,               XK_F3,                      autoselect,     { .v = "email"       }},
    { MODKEY,               XK_F4,                      autoselect,     { .v = "explorer"    }},
    { MODKEY,               XK_F5,                      autoselect,     { .v = "player"      }},
    { MODKEY,               XK_F6,                      spawn,          { .v = "osd ~/.bin/rebase" }},
    { MODKEY,               XK_F7,                      spawn,          { .v = "statnot_ctrl Timer" }},
    { MODKEY,               XK_F8,                      spawn,          { .v = "osd ~/.bin/monitor auto" }},
    { MODKEY,               XK_F9,                      spawn,          { .v = "osd ~/.bin/keyboard auto" }},
    { MODKEY,               XK_F10,                     spawn,          { .v = "statnot_ctrl Recall" }},
    { MODKEY,               XK_F11,                     spawn,          { .v = "hibernate"   }},
    { MODKEY,               XK_F12,                     spawn,          { .v = "wallpaper"   }},
    { MODKEY,               XK_BackSpace,               spawn,          { .v = "pass_ask"    }},
    { MODKEY|ShiftMask,     XK_BackSpace,               spawn,          { .v = "priv_ask"    }},
    { MODKEY,               XK_Escape,                  spawn,          { .v = "status"      }},
    { MODKEY,               XK_l,                       spawn,          { .v = "lock"        }},
    { MODKEY|AltMask,       XK_l,                       spawn,          { .v = "screen_off"  }},
    { MODKEY|ControlMask,   XK_l,                       spawn,          { .v = "dpms toggle" }},
    { MODKEY|ControlMask,   XK_x,                       spawn,          { .v = "statnot_ctrl ToggleSilent" }},
    { MODKEY,               XK_x,                       toggletitle,    { 0                  }},
    { MODKEY,               XK_h,                       spawn,          { .v = "help"        }},
    { MODKEY,               XK_q,                       spawn,          { .v = "dwm_ctrl quit"}},
    { MODKEY|ControlMask,   XK_q,                       quit,           { 0                  }},
    { ShiftMask,            XF86XK_PowerOff,            spawn,          { .v = "shutdown"    }},
    { ControlMask,          XF86XK_PowerOff,            spawn,          { .v = "reboot"      }},
    { ShiftMask,            XF86XK_Sleep,               spawn,          { .v = "hibernate"   }},
    { MODKEY,               XK_a,                       alltags,        { 0                  }},
    { MODKEY,               XK_b,                       togglebar,      { 0                  }},
    { MODKEY,               XK_j,                       focusstack,     { .i = +1            }},
    { MODKEY,               XK_k,                       focusstack,     { .i = -1            }},
    { MODKEY|ShiftMask,     XK_j,                       cycle,          { .i = +1            }},
    { MODKEY|ShiftMask,     XK_k,                       cycle,          { .i = -1            }},
    { MODKEY,               XK_Left,                    movekb,         { .i = XK_Left       }},
    { MODKEY,               XK_Right,                   movekb,         { .i = XK_Right      }},
    { MODKEY,               XK_Up,                      movekb,         { .i = XK_Up         }},
    { MODKEY,               XK_Down,                    movekb,         { .i = XK_Down       }},
    { MODKEY|AltMask,       XK_Left,                    resizekb,       { .i = XK_Left       }},
    { MODKEY|AltMask,       XK_Right,                   resizekb,       { .i = XK_Right      }},
    { MODKEY|AltMask,       XK_Up,                      resizekb,       { .i = XK_Up         }},
    { MODKEY|AltMask,       XK_Down,                    resizekb,       { .i = XK_Down       }},
    { MODKEY|ControlMask,   XK_Left,                    setmfact,       { .f = -0.05         }},
    { MODKEY|ControlMask,   XK_Right,                   setmfact,       { .f = +0.05         }},
    { MODKEY,               XK_Return,                  zoom,           { 0                  }},
    { MODKEY,               XK_Tab,                     view,           { 0                  }},
    { AltMask,              XK_Tab,                     focusstack,     { .i = +1            }},
    { AltMask|ShiftMask,    XK_Tab,                     focusstack,     { .i = -1            }},
    { MODKEY,               XK_c,                       killclient,     { 0                  }},
    { MODKEY,               XK_s,                       cyclespawntype, { .i = +1            }},
    { MODKEY|ShiftMask,     XK_s,                       setlayout,      { 0                  }},
    { MODKEY,               XK_space,                   togglefloating, { 0                  }},
    { MODKEY,               XK_comma,                   focusmon,       { .i = -1            }},
    { MODKEY,               XK_period,                  focusmon,       { .i = +1            }},
    { MODKEY|ShiftMask,     XK_comma,                   tagmon,         { .i = -1            }},
    { MODKEY|ShiftMask,     XK_period,                  tagmon,         { .i = +1            }},
    { MODKEY,               XK_grave,                   movetofree,     { .i = 0             }},
    { MODKEY|ShiftMask,     XK_grave,                   movetofree,     { .i = 1             }},
    // QWERTY
    { MODKEY,               XK_0,                       view,           { .ui = ~0           }},
    { MODKEY|ShiftMask,     XK_0,                       tag,            { .ui = ~0           }},
    TAGKEYS(                XK_1,       0)
    TAGKEYS(                XK_2,       1)
    TAGKEYS(                XK_3,       2)
    TAGKEYS(                XK_4,       3)
    TAGKEYS(                XK_5,       4)
    TAGKEYS(                XK_6,       5)
    TAGKEYS(                XK_7,       6)
    TAGKEYS(                XK_8,       7)
    // AZERTY
    { MODKEY,               0xe0,                       view,           { .ui = ~0           }},
    { MODKEY|ShiftMask,     0xe0,                       tag,            { .ui = ~0           }},
    TAGKEYS(                0x26,       0)
    TAGKEYS(                0xe9,       1)
    TAGKEYS(                0x22,       2)
    TAGKEYS(                0x27,       3)
    TAGKEYS(                0x28,       4)
    TAGKEYS(                0x2d,       5)
    TAGKEYS(                0xe8,       6)
    TAGKEYS(                0x5f,       7)
};

/* button definitions */
static Button buttons[] = {
    /* click                event mask  button          function        argument */
    { ClkAny,               0,          ButtonMod,      modmouse,       { 0                  }},
    { ClkAny,               MODKEY,     Button4,        cycle,          { .i = -1            }},
    { ClkAny,               MODKEY,     Button5,        cycle,          { .i = +1            }},
    { ClkAny,               MODKEY,     Button8,        killclient,     { 0                  }},
    { ClkClient,            MODKEY,     Button6,        movetofree,     { 0                  }},
    { ClkClient,            MODKEY,     Button7,        movetofree,     { 0                  }},
    { ClkClient,            0,          Button6,        keycode,        { .i = XK_F8         }},
    { ClkClient,            0,          Button7,        keycode,        { .i = XK_F9         }},
    { ClkBarAny,            0,          Button6,        spawn,          { .v = "brightness down"}},
    { ClkBarAny,            0,          Button7,        spawn,          { .v = "brightness up"}},
    { ClkBarNotif,          0,          Button1,        spawn,          { .v = "statnot_ctrl Click"}},
    { ClkBarDate,           0,          Button1,        spawn,          { .v = "calendar"    }},
    { ClkBarDate,           0,          Button2,        spawn,          { .v = "screen_off"  }},
    { ClkBarDate,           0,          Button3,        spawn,          { .v = "dpms toggle" }},
    { ClkBarDate,           MODKEY,     ButtonMod,      spawn,          { .v = "statnot_ctrl Timer"}},
    { ClkBarMode,           0,          Button1,        spawn,          { .v = "statnot_ctrl ToggleSilent"}},
    { ClkBarMode,           0,          Button3,        toggletitle,    { 0                  }},
#if COUNTERS > 0
    { ClkBarCounter1,       0,          Button1,        spawn,          { .v = "dwm_ctrl counter1_b1" }},
    { ClkBarCounter1,       0,          Button2,        spawn,          { .v = "dwm_ctrl counter1_b2" }},
    { ClkBarCounter1,       0,          Button3,        spawn,          { .v = "dwm_ctrl counter1_b3" }},
#if COUNTERS > 1
    { ClkBarCounter2,       0,          Button1,        spawn,          { .v = "dwm_ctrl counter2_b1" }},
    { ClkBarCounter2,       0,          Button2,        spawn,          { .v = "dwm_ctrl counter2_b2" }},
    { ClkBarCounter2,       0,          Button3,        spawn,          { .v = "dwm_ctrl counter2_b3" }},
#if COUNTERS > 2
    { ClkBarCounter3,       0,          Button1,        spawn,          { .v = "dwm_ctrl counter3_b1" }},
    { ClkBarCounter3,       0,          Button2,        spawn,          { .v = "dwm_ctrl counter3_b2" }},
    { ClkBarCounter3,       0,          Button3,        spawn,          { .v = "dwm_ctrl counter3_b3" }},
#if COUNTERS > 3
    { ClkBarCounter4,       0,          Button1,        spawn,          { .v = "dwm_ctrl counter4_b1" }},
    { ClkBarCounter4,       0,          Button2,        spawn,          { .v = "dwm_ctrl counter4_b2" }},
    { ClkBarCounter4,       0,          Button3,        spawn,          { .v = "dwm_ctrl counter4_b3" }},
#if COUNTERS > 4
    { ClkBarCounter5,       0,          Button1,        spawn,          { .v = "dwm_ctrl counter5_b1" }},
    { ClkBarCounter5,       0,          Button2,        spawn,          { .v = "dwm_ctrl counter5_b2" }},
    { ClkBarCounter5,       0,          Button3,        spawn,          { .v = "dwm_ctrl counter5_b3" }},
#endif
#endif
#endif
#endif
#endif
    { ClkBarAny,            0,          Button4,        spawn,          { .v = "volume up"   }},
    { ClkBarAny,            0,          Button5,        spawn,          { .v = "volume down" }},
    { ClkBarTag,            0,          Button1,        view,           { 0                  }},
    { ClkBarTag,            0,          Button3,        toggleview,     { 0                  }},
    { ClkBarTag,            MODKEY,     Button1,        tag,            { 0                  }},
    { ClkBarTag,            MODKEY,     Button3,        toggletag,      { 0                  }},
    { ClkBarLayout,         0,          Button1,        setlayout,      { 0                  }},
    { ClkBarLayout,         0,          Button3,        cyclespawntype, { .i = +1            }},
    { ClkBarControl,        0,          Button1,        spawn,          { .v = "lock"        }},
    { ClkBarControl,        0,          Button3,        spawn,          { .v = "hibernate"   }},
    { ClkClient,            MODKEY,     Button2,        zoom,           { 0                  }},
    { ClkRoot,              0,          Button1,        spawn,          { .v = "wallpaper"   }},
    { ClkClient,            MODKEY,     Button1,        movemouse,      { 0                  }},
    { ClkClient,            MODKEY,     Button2,        togglefloating, { 0                  }},
    { ClkClient,            MODKEY,     Button3,        resizemouse,    { 0                  }},
};
