# dwm version
VERSION = 6.0

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

X11INC = /usr/X11R6/include
FTINC = /usr/include/freetype2
X11LIB = /usr/X11R6/lib

# Xinerama
XINERAMALIBS = -L${X11LIB} -lXinerama
XINERAMAFLAGS = -DXINERAMA

# includes and libs
INCS = -I. -I/usr/include -I${X11INC} -I${FTINC}
LIBS = -L/usr/lib -lc -L${X11LIB} -lX11 ${XINERAMALIBS} -lXft

BORDERPX?=1
FONT?="Dejavu Sans Mono:medium:size=11"
GAPPX?=6
COUNTERS?=0

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\" ${XINERAMAFLAGS} -DBORDERPX=${BORDERPX} -DFONT="\"${FONT}\"" -DGAPPX=${GAPPX} -DCOUNTERS=${COUNTERS}
CFLAGS = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os ${INCS} ${CPPFLAGS}
LDFLAGS = -s ${LIBS}

# compiler and linker
CC = cc
